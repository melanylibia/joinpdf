import subprocess
import sys
import os
from flask import Flask, send_file, request, Response, send_from_directory
from tempfile import mkdtemp
from shutil import rmtree
from ntpath import basename, splitext

app = Flask(__name__)

created_directories = []

def add_suffix(file):
    base, ext = splitext(basename(file))
    print(base, ext)
    return base + "_joined" + ext

@app.route("/upload", methods=['POST'])
def upload():
    files = request.files.getlist('file')
    
    if files:
        dirname = mkdtemp()
        outputfile = "final.pdf"
        res = []
        for file in files:
            if file:                
                filename = file.filename.replace(" ","")
                inputFile = dirname + f"/{filename}"
                file.save(inputFile)
                res.append(f"{dirname}/{filename}")
        cmds = ["gs", "-dNOPAUSE", "-dBATCH", 
        "-dSAFER",
        "-sDEVICE=pdfwrite",
        f"-sOutputFile={dirname}/{outputfile}"
        ]
        proc = subprocess.Popen(cmds + res, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        cmds = ["gs", "-dNOPAUSE", "-dBATCH", 
        "-dSAFER",
        "-sDEVICE=pdfwrite",
        f"-sOutputFile={dirname}/{outputfile}"
        ]
        res = [f"{dirname}/{outputfile}"]
        proc = subprocess.Popen(cmds + res, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # result = subprocess.run(cmds+res)



        def stream_out():
            try:
                outs, err = proc.communicate()[0]
                print('starting')
                yield outs
            except TimeoutExpired:
                proc.kill()
                print('exiting')
                outs, err = proc.communicate()
            finally:
                print(dirname)
                # rmtree(dirname)s

        resp = Response(stream_out(), mimetype='application/pdf')
        resp.headers['Content-Type'] = 'application/pdf'
        resp.headers['Content-Disposition'] = 'attachment; filename="' + add_suffix(outputfile) + '"'
        return res

    return 'no file given'

@app.route('/')
def send_index():
    return send_from_directory('public', 'index.html')


@app.route('/<path:path>')
def send_static(path):
    return send_from_directory('public', path)

if __name__ == "__main__":
    port = int(os.environ.get('PORT',5000))
    app.run(host='0.0.0.0', port=port)
